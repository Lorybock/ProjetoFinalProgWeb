@extends('default')

@section('content')
<div class="container-fluid">
            <hr/>
            <h3 class="page-item">Editar cursos</h3>
            <div class="col-md-12"
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('course/') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Editar</li>
                    </ol>
                </nav>
            </div>
            <hr/>
            <form method="post" 
            action="{{route('course.update', $course->id)}}" 
                          enctype="multipart/form-data">
                          {!! method_field('put') !!}
                        {{ csrf_field() }}

                <div id="acoes" class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="inputCode">Codigo</label>
                            <input type="text" class="form-control" id="inputCode" name="code" value="{{$course->code or old('code')}}" required>
                        </div>
                        <div class="form-group col-md-7">
                        <label for="inputName">Titulo</label>
                        <input type="text" class="form-control" id="inputName" name="name" value="{{$course->name or old('name')}}" required>
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="inputInstructor">Nome do Instrutor</label>
                        <input type="text" class="form-control" id="inputInstructor" name="instructor" value="{{$course->instructor or old('instructor')}}" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="inputCreditHour">Carga Horaria</label>
                            <input type="number" min="0" class="form-control" id="inputCreditHour" name="credit_hour" value="{{$course->credit_hour or old('credit_hour')}}" required>
                        </div>
                        
                        <div class="form-group col-md-5">
                            <label for="inputPrice">Preço</label>
                            <!-- <input type="text" class="form-control" id="inputPrice" name="price" value="{{$course->price or old('price')}}" required> -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">R$</span>
                                </div>
                                <input type="number" min="0" class="form-control" id="inputPrice" name="price" value="{{$course->price or old('price')}}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDescription">Descrição do Curso</label>
                        <textarea class="form-control" id="inputDescription" rows="3" name="description" required>{{$course->description or old('description')}}</textarea>
                    </div>  
                </div>
                <div class="col-md-12">                   
                    <button type="reset" class="btn btn-danger">Limpar</button>
                    <button type="submit" class="btn btn-warning" id="black">Alterar</button>
                </div>

                <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
            </form>
        </div>
@stop
