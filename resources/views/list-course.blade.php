@extends('default')

@section('content')
@if (session('message'))
    <div id="alertaMessage" class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
    </div>
@endif

<div class="container-fluid">
    <hr/>
    <div id="topo" class="row">
        <div class="col-md-10">
            <h2>Itens</h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ url('course/create') }}">Novo Curso</a>
        </div>
        <div class="col-md-12"
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Home</li>
                </ol>
            </nav>
        </div>
    </div>
    <hr/>
    <div id="lista" class="row">
        <div class="table-responsive col-md-12">
            <table class="table table-bordered table-striped" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                    <th scope="col">Codigo</th>
                    <th scope="col">Titulo</th>
                    <th scope="col">Instrutor</th>
                    <th scope="col">CH</th>
                    <th scope="col">Preço</th>
                    <th scope="col">Descrição</th>
                    <th class="acoes" scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($courses as $course)   
                    <tr>
                    <th scope="row">{{$course->code}}</th>
                    <td>{{$course->name}}</td>
                    <td>{{$course->instructor}}</td>
                    <td>{{$course->credit_hour}}</td>
                    <td>{{$course->price}}</td>
                    <td>{{$course->description}}</td>
                    <td>
                    <a class="btn btn-warning btn-sm" href="{{route('course.edit', $course->id)}}">Editar</a>
                    &nbsp;<form style="display: inline-block;" method="POST" 
                                                        action="{{route('course.destroy', $course->id)}}"                                                        
                                                        data-toggle="tooltip" data-placement="top"
                                                        title="Excluir" 
                                                        onsubmit="return confirm('Confirma exclusão?')">
                                                {{method_field('DELETE')}}{{ csrf_field() }}     
                                                <button type="submit" class="btn btn-danger btn-sm">Excluir
                                                    </form>                                           
                                                </button>
                                                       
                    </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>

@stop
