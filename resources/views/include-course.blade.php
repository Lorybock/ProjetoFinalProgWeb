@extends('default')

@section('content')
<div class="container-fluid">
            <hr/>
            <h3 class="page-item">Adicionar cursos</h3>
            <div class="col-md-12"
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('course/') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Adicionar</li>
                    </ol>
                </nav>
            </div>
            <hr/>
            <form method="post" 
                          action="{{route('course.store')}}" 
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                <div id="acoes" class="col-md-12">
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="inputCode">Codigo</label>
                            <input type="text" class="form-control" id="inputCode" name="code" aria-describedby="codeHelp" placeholder="Digite o código">
                        </div>
                        <div class="form-group col-md-7">
                        <label for="inputName">Titulo</label>
                        <input type="text" class="form-control" id="inputName" name="name" aria-describedby="nameHelp" placeholder="Digite o título">
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="inputInstructor">Nome do Instrutor</label>
                        <input type="text" class="form-control" id="inputInstructor" name="instructor" aria-describedby="instructorHelp" placeholder="Digite o nome do instrutor">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="inputCreditHour">Carga Horaria</label>
                            <input type="number" min="0" class="form-control" id="inputCreditHour" name="credit_hour" aria-describedby="creditHourHelp" placeholder="100">
                        </div>
                        
                        <div class="form-group col-md-5">
                            <label for="inputPrice">Preço</label>
                            <!-- <input type="text" class="form-control" id="inputPrice" name="price" aria-describedby="PriceHelp" placeholder="Digite o preço do Curso"> -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">R$</span>
                                </div>
                                <input type="number" min="0" class="form-control" aria-label="Amount (to the nearest dollar)" id="inputPrice" name="price" aria-describedby="PriceHelp" placeholder="00">
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                    <div class="form-group">
                        <label for="inputDescription">Descrição do Curso</label>
                        <textarea class="form-control" id="inputDescription" rows="3" name="description"></textarea>
                    </div>  
                </div>
                <div class="col-md-12">                   
                    <button type="reset" class="btn btn-danger">Limpar</button>
                    <button type="submit" class="btn btn-warning" id="black">Cadastrar</button>
                </div>

                <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
            </form>
        </div>
@stop
