<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
class CourseController extends Controller
{
    //
    public function index() {
        $courses = Course::all();
        $total = Course::all()->count();
        return view('list-course', compact('courses', 'total'));
    }

    public function create() {
        return view('include-course');
    }

    public function store(Request $request) {
        $course = new Course;
        $course->id = $request->id;
        $course->code = $request->code;
        $course->name = $request->name;
        $course->description = $request->description;
        $course->credit_hour = $request->credit_hour;
        $course->instructor = $request->instructor;
        $course->price = $request->price;
        $course->save();
        return redirect()->route('course.index')->with('message','Curso criado com sucesso');
        
    }

    public function show($id){

    }

    public function edit($id){
        $course = Course::findOrFail($id);
        return view('alter-course', compact('course'));
    }

    public function update(Request $request, $id){
        $course = Course::findOrFail($id);
        $course->code = $request->code;
        $course->name = $request->name;
        $course->description = $request->description;
        $course->credit_hour = $request->credit_hour;
        $course->instructor = $request->instructor;
        $course->price = $request->price;
        $course->save();
        return redirect()->route('course.index')->with('message','Curso alterado com sucesso!');
        
    }

    public function destroy($id){
        $course = Course::findOrFail($id);
        $course->delete();
        return redirect()->route('course.index')->with('message', 'Curso excluido com sucesso!');
    }


}
